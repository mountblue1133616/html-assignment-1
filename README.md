# Team Page

This is a responsive HTML/CSS website showcasing various team members along with their names, roles, and photos. The purpose of this website is to provide an overview of the team and introduce its members to visitors.

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)

## Features

- Display team members with their names, roles, and photos.
- Responsive design that adapts to different screen sizes and devices.
- Clean and modern user interface.

## Installation

1. Clone the repository:

```shell
git clone git@gitlab.com:mountblue1133616/html-assignment-1.git
```

2. Navigate to the project directory


## Usage

1. Open the `index.html` file in your web browser.
2. You will see a responsive team page with the names, roles, and photos of various team members.
3. Scroll or resize the browser window to observe the responsive behavior of the website.

## Contributing

Contributions to this project are welcome. If you find any issues or have suggestions for improvement, please open an issue or submit a pull request.

1. Fork the repository.
2. Create your branch: `git checkout -b feature/your-feature`
3. Commit your changes: `git commit -am 'Add your feature'`
4. Push to the branch: `git push origin feature/your-feature`
5. Submit a pull request.
